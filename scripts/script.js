$(document).ready(function () {


    var communityRecentActivityList = $('.community-recent-activity__list');
    var getData = function () {
        $.ajax({
            url: 'https://jsonplaceholder.typicode.com/posts/',
            success: function (res, textStatus, jqXHR) {
                printData(res);
                initSlider();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('AJAX call failed.');
                console.log(textStatus + ': ' + errorThrown);
            },
            complete: function () {
                console.log('AJAX call completed');
            }
        })
    };
    var printData = function (res) {
        for (i = 0; i < 10; i++) {
            var data = res[i];
            communityRecentActivityList.prepend('<div class="community-recent-activity-item"><span class="community-recent-activity-item__title">' + data.title + '</span><p class="community-recent-activity-item__content">' + data.body + '</p></div>')
        }
    };
    var initSlider = function () {
        communityRecentActivityList.slick({
            infinite: false,
            dots: true,
            arrows: false,
            slidesToShow: 4,
            slidesToScroll: 4,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    }
                }

            ]
        });
    };
    getData();
});